all: tick5.deb

.PHONY: all

tick5.deb: buildroot/usr/bin/tick5 buildroot/DEBIAN/control
	dpkg-deb --build buildroot tick5.deb

buildroot/usr/bin/tick5: buildroot/usr/bin
	cp -pdf tick5 buildroot/usr/bin/tick5

buildroot/DEBIAN/control: buildroot/DEBIAN
	cp -pdf tick5.control buildroot/DEBIAN/control

buildroot/usr/bin:
	mkdir -p $@

buildroot/DEBIAN:
	mkdir -p $@

clean:
	rm -Rf buildroot

.PHONY: clean
