%define name tick5
%define systemddir /lib/systemd/system
%define version 0.1

# Some metadata required by an RPM package
Name: tick5
Summary: Print a message every 5s
Version: %version
Release: 1
License: MIT


%description
tick5 is a simple useless script that echos a message every 5 seconds.


%build
mkdir -p %{buildroot}%{_prefix}/bin
mkdir -p %{buildroot}%{systemddir}
cp -pdf %(echo $PWD)/%{name} %{buildroot}%{_prefix}/bin/%{name}
chmod 755 %{buildroot}%{_prefix}/bin/%{name}

# generate systemd service file.
cat <<EOF> %{buildroot}%{systemddir}/%{name}.service
[Unit]

[Install]
WantedBy=multi-user.target

[Service]
ExecStart=%{_prefix}/bin/%{name}
Restart=always
RestartSec=5
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=%n
EOF

%files
%{_prefix}/bin/%{name}
%{systemddir}/%{name}.service
